package com.progoti.npsb;

import com.progoti.npsb.jpos.SendTestMessage;
import org.jpos.iso.ISOException;
import org.jpos.q2.Q2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@SpringBootApplication
public class NPSBServerSimulator {

	public static void main(String[] args) {
		SpringApplication.run(NPSBServerSimulator.class, args);
		Q2 q2 = new Q2();
		Thread t = new Thread(q2);
		t.start();
	}


	@GetMapping("send-request-message-to-tallypay")
	public String sendRequestMessage() throws ISOException {
		try {
			new SendTestMessage().startService();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "done";
	}

}
