package com.progoti.npsb.jpos;

import org.jpos.iso.*;

import java.io.IOException;
import java.util.Random;

import com.progoti.npsb.util.StringUtil;


public class NPSBRequestListener implements ISORequestListener {

    @Override
    public boolean process(ISOSource isoSource, ISOMsg isoMsg) {

        try {
            if (isoMsg.isRequest()) {
                Random random = new Random(System.currentTimeMillis());
                boolean isOdd = (Math.abs(random.nextInt()) % 2) == 1;

                byte[] header = isoMsg.getHeader();
                if (header != null && header.length > 0) {
                    System.out.println("-----------------------------------------------------------------");
                    System.out.println("Header: [" + StringUtil.bytesToHex(header) + "]");
                    System.out.println("-----------------------------------------------------------------");
                } else {
                    System.out.println("---------------- No header found -------------------");
//                System.out.println(ISOUtil.hexdump(isoMsg.getBytes()));
                    isoMsg.dump(System.out, "");
                }

                isoMsg.setResponseMTI();
                isoMsg.set(39, isOdd ? "01" : "00");
                isoSource.send(isoMsg);

                return true;
            } else {
                return false;
            }
        } catch (ISOException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
