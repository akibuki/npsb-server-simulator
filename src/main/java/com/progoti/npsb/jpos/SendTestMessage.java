package com.progoti.npsb.jpos;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOServer;
import org.jpos.q2.QBeanSupport;
import org.jpos.q2.iso.QMUX;
import org.jpos.util.NameRegistrar;


public class SendTestMessage extends QBeanSupport {

    private ISOServer isoServer;

    @Override
    public void startService() throws Exception {

        QMUX mux = (QMUX) NameRegistrar.getIfExists("mux.npsb-mux");
        isoServer = ISOServer.getServer("npsb-server-9091");
        if (isoServer.getConnections() > 0 && mux != null) {
            try {
//              if (mux.isConnected()) {
                    ISOMsg m = new ISOMsg();
                    /*
                    m.setMTI("0100");
                    m.set(2, "123456789012345");
                    m.set(11, "901234");
                    m.set(41, "12345678");
                    */

                    m.setMTI("0100");
//                    m.set(2, "0001750000000000");
                    m.set(2, "1750000000000");
                    m.set(3, "280000");
                    m.set(4, "000000010000");
                    m.set(7, "0929045408");
                    m.set(11, "496719");
                    m.set(12, "105408");
                    m.set(13, "0929");
                    m.set(18, "4111");
                    m.set(22, "000");
                    m.set(32, "000125");
                    m.set(37, "127210496719");
                    m.set(41, "IBNK0000");
//                    m.set(42, "[10039004       ]");
                    m.set(42, "10039004");
                    m.set(43, "IBBL Online UAT          Dhaka        BD");
                    m.set(47, "3932373031313031393132323033363032");
                    m.set(49, "050");
                    m.set(103, "3555101142997");
                    m.set(112, "F012D20954574841545F545258D3055451524D50");

//                    String data = ISOUtil.hexdump(m.pack());
//                    System.out.println(data);
                    mux.request(m, 5000);
//              }

            } catch (ISOException e) {
                e.printStackTrace();
            }
        }
    }

}